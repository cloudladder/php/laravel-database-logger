<?php

namespace Gupo\DatabaseLogger\Supports;

use Illuminate\Database\Events\QueryExecuted;
use Gupo\DatabaseLogger\Contracts\ResolvingResultContract;
use Illuminate\Support\Arr;

/**
 * SQL解析结果
 */
class ResolvingResult implements ResolvingResultContract
{
    /**
     * 原始查询
     *
     * @var QueryExecuted
     */
    protected $rawQuery;

    /**
     * 执行的SQL语句
     *
     * @var string
     */
    protected $executeSql;

    /**
     * @param QueryExecuted $rawQuery 原始查询
     * @param string $executeSql 执行的SQL语句
     */
    public function __construct(QueryExecuted $rawQuery, string $executeSql)
    {
        $this->rawQuery = $rawQuery;
        $this->executeSql = $executeSql;
    }

    /**
     * 获取 执行SQL
     *
     * @return string
     */
    public function getExecuteSql()
    {
        return $this->executeSql;
    }

    /**
     * 获取 原始查询
     *
     * @return QueryExecuted
     */
    public function getRawQuery()
    {
        return $this->rawQuery;
    }

    /**
     * 获取 执行耗时
     *
     * @return int|float
     */
    public function getExecuteTime()
    {
        $rawQuery = $this->getRawQuery();

        return !empty($rawQuery) ? $rawQuery->time * 1000 : 0;
    }

    /**
     * 获取 格式化后的执行耗时
     *
     * @return string
     */
    public function getFormatExecuteTime()
    {
        $milliseconds = $this->getExecuteTime();

        if ($milliseconds < 1000) {
            $milliseconds = round($milliseconds) . 'μs';
        } elseif ($milliseconds < 10000000) {
            $milliseconds = round($milliseconds / 1000, 2) . 'ms';
        } else {
            $milliseconds = round($milliseconds / 1000 / 1000, 2) . 's';
        }

        return $milliseconds;
    }

    /**
     * 获取 调用栈
     *
     * @return string
     */
    public function getCallTraces()
    {
        if (!config('database-logger.show_traces', false)) {
            return '';
        }

        $traces = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 30);

        if (empty($traces)) {
            return '';
        }

        $position = Arr::first(
            $traces,
            function ($item) {
                return !mb_strpos($item['file'], '/vendor/');
            },
            $traces[count($traces) - 1]
        );

        return sprintf(
            '%s:%s',
            str_replace(base_path() . DIRECTORY_SEPARATOR, '', $position['file']),
            $position['line']
        );
    }
}
