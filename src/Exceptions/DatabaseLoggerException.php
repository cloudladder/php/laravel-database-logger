<?php

namespace Gupo\DatabaseLogger\Exceptions;

class DatabaseLoggerException extends \Exception implements \Throwable
{
}
